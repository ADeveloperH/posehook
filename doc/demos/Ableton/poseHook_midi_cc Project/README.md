# Demo MIDI CC in Ableton LIVE

To demonstrate poseHook, here is a set containing a Max for Live device which demonstrates
2 different techinques. Demo 1 on the left of the device is a stabilized trigger on/off, demo 2
on the right shows continous MIDI cc output.

![Ableton screen shot](../../images/midicc.PNG)

[Download the Live set](https://bitbucket.org/constanze/posehook/raw/master/doc/demos/poseHook_midi_cc%20Project.zip)

## Installation

 - find out the IP adress of your host where Ableton live runs, e.g. 192.168.43.124
 - take care that the host and the phone are in the same Wifi, and no firewall blocks communication from phone to host
 - start poseHook app on the phone
 - select menu, _IP and port_, enter the IP adress, change the port to e.g. 10627
 - You need to set up a virtual MIDI bus. Instructions are [here](https://help.ableton.com/hc/en-us/articles/209774225-Using-virtual-MIDI-buses)
 - Run Ableton Live, and open the Live Set
 - in the M4L device, click into the OSC port textarea (should be the same as in the app, e.g.10627) and hit the return key.
 - In the poseHook track, configure _MIDI To_ using the virtual MIDI bus
 - Likewise, the Chord track, configure _MIDI From_ using the virtual MIDI bus

On Windows, using loopMIDI it looks like this

![another Ableton screen shot](../../images/loopback.png)

## Running the App

Incoming data from the app is indicated by a little blinking dot near the OSC port.
Best you use a large mirror and the back camera for a first test. If at least your upper
body is visible in the camera image, results are better. Also, try various values of the setting
_Minimum confidence filter_ on the phone and see the result.

### Demo 1

The device demonstrates how to control noisy data, or how to prevent reactions by accidental moves.

Raise your left hand (the right one if you are using a mirror) above the left (right) elbow,
and watch how the green bar in the device grows. If you keep the hand up in the air until the
green bar is full, the trigger goes on, and the Chord track gets started. Put the hand down, and again
after some delay the trigger goes off, the Chord track is silenced.

The device reacts only after counting _n_ measurements in the same range.
This introduces latency, but avoids flickering. You can change the number _n_ in the
field _Trigger delay_. Once trigger on is sent, the device will avoid sending another "on".
And vice versa fo "off".

### Demo 2

This simple demo outputs a continous stream of MIDI CC data. Move the right hand horizontaly to see
the effect. The middle position is the the center of the screen.

If you have to stand off-center, you can adjust the high/low input values of the _scale_ in the
max patch.

--------------------

poseHook is licensed under the Apache License 2.0. Source code at https://bitbucket.org/constanze/posehook/





