# Demo Note on/off in Ableton LIVE

To demonstrate poseHook, here is a set containing a little Max for Live device.

![Ableton screen shot](../../images/noteonoff.PNG)


[Download the Live set](https://bitbucket.org/constanze/posehook/raw/master/doc/demos/poseHook_note_onoff%20Project.zip)

The device creates a MIDI Note On, as soon as poseHook detects that the right hand
is raised higher than the nose. And Note Off when the y coordinate of
the hand is less than the one of the nose.

This is how it works:

 - find out the IP adress of your host where Ableton live runs, e.g. 192.168.43.124
 - take care that the host and the phone are in the same Wifi, and no firewall blocks communication from phone to host
 - start poseHook app on the phone
 - select menu, _IP and port_, enter the IP adress, change the port to e.g. 10627
 - Run Ableton Live, and open the Live Set
 - in the M4L device, click into the port textarea (should be the same as in the app, e.g.10627) and hit the return key.

You should now see incoming data, indicated by a little blinking dot near the port textarea.

Best you use a large mirror and the back camera for a first test. Raise your
hand (the left one if you are using a mirror) and play around with the distance from the
mirror until you get the most reliable results. If your upper body is visible in the camera image,
results are better. Also, try various values of the setting
_Minimum confidence filter_ on the phone and see the result.

--------------------

poseHook is licensed under the Apache License 2.0. Source code at https://bitbucket.org/constanze/posehook/





