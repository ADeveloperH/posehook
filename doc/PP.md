# Privacy Policy for poseHook

poseHook works completely offline, i.e. it does not make use of online resource for detection. 
The only data poseHook transmits is the intended OSC messages, which is 100% under you control.
No data is collected or shared whatsoever. Your data is yours. 
