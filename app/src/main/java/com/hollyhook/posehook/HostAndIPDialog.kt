package com.hollyhook.posehook

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment

class HostAndIPDialog : DialogFragment() {
    private val TAG = "SettingsDialog"

    // Use this instance of the interface to get members of the parent
    private lateinit var parent: MainActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            parent = context as MainActivity
        } catch (e: ClassCastException) {
            Log.e(TAG, "invalid parent class")
        }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val factory = LayoutInflater.from(context)

        // add a custom layout to an AlertDialog
        val iPEntryView: View = factory.inflate(R.layout.dialog_edit_ip, null)

        // restore ip
        val oldip: String? = parent.sharedPref!!.getString(
          getString(R.string.ip_key),
          getString(R.string.ip_default)
        )
        (iPEntryView.findViewById(R.id.ip_edit) as EditText).setText(oldip)
        var ip = oldip

        // likewise for port
        val oldport: Int = parent.sharedPref!!.getInt(
          getString(R.string.port_key),
          getString(R.string.port_default).toInt()
        )
        (iPEntryView.findViewById(R.id.port_edit) as EditText).setText(oldport.toString())
        var port = oldport

        return AlertDialog.Builder(context, R.style.CustomDialogTheme)
            .setView(iPEntryView)
            .setPositiveButton(R.string.alert_dialog_ok) { _, _ ->

                // ok button clicked
                val resIp: CharSequence =
                    (iPEntryView.findViewById<View>(R.id.ip_edit) as TextView).text
                val resPort: CharSequence =
                    (iPEntryView.findViewById<View>(R.id.port_edit) as TextView).text
                try {
                    ip = resIp.toString().trim()
                    port = resPort.toString().trim().toInt()
                } catch (e: java.lang.Exception) {
                    // don't change port
                    val toast = Toast.makeText(context, R.string.error_port, Toast.LENGTH_LONG)
                    toast.show()
                }
                // save values
                val editor: SharedPreferences.Editor = parent.sharedPref!!.edit()
                editor.putString(getString(R.string.ip_key), ip)
                editor.putInt(getString(R.string.port_key), port)
                editor.apply()
            }
            .create()
    }
}