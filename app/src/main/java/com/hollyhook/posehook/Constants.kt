
@file:JvmName("Constants")

package com.hollyhook.posehook

/** Request camera and external storage permission.   */
const val REQUEST_CAMERA_PERMISSION = 1
const val REQUEST_EXTSTORAGE_PERMISSION = 2

/** how many data points to hold in a series */
const val DATA_LEN = 500 // for 30 sec that would be enough if 16fps

const val PLOT_SHAPE_SIZE = 5
const val PLOT_BODY_PART_DEFAULT = 0 // nose

const val MIRROR_OSC_DEFAULT = true

const val ONE_FLOAT_OSC_DEFAULT = false
const val COMBINE_OSC_DEFAULT = true
const val COMBINE_OSC_ADDRESS_DEFAULT = "/posehook/%a"

const val TAKE_FRONT_CAMERA_DEFAULT = true

const val NUM_OF_VALUES_PER_LANDMARK = 4 // must be 3 at least
const val NUM_OF_RECORDED_MESSAGES = 30


const val NOSE_ON_DEFAULT = true
const val EYES_ON_DEFAULT = false
const val EARS_ON_DEFAULT = false
const val MOUTH_ON_DEFAULT = false
const val SHOULDER_ON_DEFAULT = false
const val ELBOWS_ON_DEFAULT = false
const val LEFT_HAND_ON_DEFAULT = false
const val RIGHT_HAND_ON_DEFAULT = false
const val HIPS_ON_DEFAULT = false
const val KNEES_ON_DEFAULT = false
const val LEFT_FOOT_ON_DEFAULT = false
const val RIGHT_FOOT_ON_DEFAULT = false

val COMBINE_OSC_ADDRESS_NAMES = arrayOf(
    "kirk",
    "spock",
    "scott",
    "zulu",
    "rand",
    "mccoy",
    "uhura",
    "chapel",
    "chekov"
)
