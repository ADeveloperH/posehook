package com.hollyhook.posehook

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences.Editor
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Switch
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.DialogFragment
import com.hollyhook.posehook.BodyPartSetup.Companion.bodyPartSetups


class OSCAddressDialog : DialogFragment() {
    private val TAG = "OSCAddressDialog"

    // Use this instance of the interface to get members of the parent
    private lateinit var parent: MainActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            parent = context as MainActivity
        } catch (e: ClassCastException) {
            Log.e(TAG, "invalid parent class")
        }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val factory = LayoutInflater.from(context)

        // add a custom layout to an AlertDialog
        val oscAddressView: View = factory.inflate(R.layout.dialog_edit_osc, null)

        val switch1 = oscAddressView.findViewById(R.id.one_float_osc) as SwitchCompat?
        switch1?.isChecked =  parent.sharedPref!!.getBoolean(
            getString(R.string.one_float_osc_key),
            ONE_FLOAT_OSC_DEFAULT
        )

        // fill in values of the check boxes and the edit fields according to what we
        // previously restored from the preferences.
        for (bp: BodyPartSetup in bodyPartSetups) {
            (oscAddressView.findViewById(bp.checkBoxID) as CheckBox).isChecked = bp.sendOSC

            for (bpc in bp.bodyPartConfigs) {
                (oscAddressView.findViewById(bpc.editId) as EditText).setText(bpc.oscAddress)
            }
        }

        return AlertDialog.Builder(context)
            .setView(oscAddressView)
            .setPositiveButton(R.string.alert_dialog_ok) { _, _ ->
                // ok button clicked, collect input data and save it in preferences
                val editor: Editor? = parent.sharedPref?.edit()
                val one = if (switch1 != null) switch1.isChecked else ONE_FLOAT_OSC_DEFAULT
                editor?.putBoolean(getString(R.string.one_float_osc_key), one)

                for (bp: BodyPartSetup in bodyPartSetups) {
                    bp.sendOSC = (oscAddressView.findViewById(bp.checkBoxID) as CheckBox).isChecked
                    editor?.putBoolean(getString(bp.sendOSCKey), bp.sendOSC)
                    for (bpc in bp.bodyPartConfigs) {
                        val p =
                            (oscAddressView.findViewById(bpc.editId) as TextView).text.toString()
                        bpc.oscAddress = p
                        editor?.putString(getString(bpc.addressKey), p)
                    }
                }

                editor?.apply()

            }
            .create()
    }
}