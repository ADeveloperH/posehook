package com.hollyhook.posehook


import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.hardware.Camera
import android.os.Bundle
import android.os.Process
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {

    private val TAG = "com.hollyhook.MainActivity"

    /** Preferences saving, like port settings etc */
    internal var sharedPref: SharedPreferences? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val permissionCamera = checkPermission(
                Manifest.permission.CAMERA, Process.myPid(), Process.myUid()
        )

        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestExtStoragePermission()
        } else if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission() // asynchronous
        } else {
            savedInstanceState ?: supportFragmentManager.beginTransaction()
                .replace(R.id.container, PoseHookFragment())
                .commit()
        }

        // check preference for stored selected context menu
        sharedPref = getSharedPreferences(TAG, Context.MODE_PRIVATE)
        BodyPartSetup.initConfig(this, sharedPref!!)
    }

    private fun requestCameraPermission() {
        if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
            Log.v(TAG, "showing permission rationale for camera")
            PermissionConfirmationDialog(
                    R.string.request_cam_permission,
                    REQUEST_CAMERA_PERMISSION,
                    Manifest.permission.CAMERA).show(supportFragmentManager, null)
        } else {
            Log.v(TAG, "request permission for camera")
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA), REQUEST_CAMERA_PERMISSION)
        }
    }


    private fun requestExtStoragePermission() {
        if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Log.v(TAG, "showing permission rationale for storage")
            PermissionConfirmationDialog(
                    R.string.request_storage_permission,
                    REQUEST_EXTSTORAGE_PERMISSION,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            ).show(supportFragmentManager, null)
        } else {
            Log.v(TAG, "request permission for storage")
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA), REQUEST_EXTSTORAGE_PERMISSION)
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        if ((requestCode == REQUEST_CAMERA_PERMISSION ||
                        requestCode == REQUEST_EXTSTORAGE_PERMISSION) &&
                allPermissionsGranted(grantResults)) {
            // move on, all if possible now
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, PoseHookFragment())
                    .commit()
            return
        }
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == REQUEST_EXTSTORAGE_PERMISSION) {
                requestCameraPermission() // need the other one
                return
            }
            if (requestCode == REQUEST_CAMERA_PERMISSION) {
                requestExtStoragePermission()
                return
            }
        }

        // we are doomed
        Log.i(TAG, "permission doom")
        PermissionErrorDialog.newInstance(getString(R.string.doom_permission))
                        .show(supportFragmentManager, null)
    }

    private fun allPermissionsGranted(grantResults: IntArray) = grantResults.all {
        it == PackageManager.PERMISSION_GRANTED
    }

    // context menu for selecting what to plot ---------------

    override fun onCreateContextMenu(
            menu: ContextMenu, v: View,
            menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_context, menu)

        // persisted value is the id of the menu entry
        val selected: Int = sharedPref!!.getInt(
                getString(R.string.context_menu_key),
                R.id.menu_nose
        )

        val menuItem = menu.findItem(selected)
        menuItem?.isChecked = true
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        return if (item.isCheckable) {
            item.isChecked = true
            val editor: SharedPreferences.Editor = sharedPref!!.edit()
            editor.putInt(getString(R.string.context_menu_key), item.itemId)
            editor.apply()
            true
        } else {
            super.onContextItemSelected(item)
        }
    }

    // action bar menu -----------------------------------------



    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)

        // disable button to switch cameras if not 2 of them
        menu.getItem(0).isVisible = hasCamera(false) && hasCamera(true)
        return true
    }

    // Handle main menu item selection
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_osc_server -> {
                // Create an instance of the dialog fragment and show it
                val dialog = HostAndIPDialog()
                dialog.show(supportFragmentManager, "SettingsDialog")
                true
            }
            R.id.action_osc_settings -> {
                val dialog = OSCConfigDialog()
                dialog.show(supportFragmentManager, "OSCConfigDialog")
                true
            }
            R.id.action_edit_confidence -> {
                val dialog = MinConfidenceDialog()
                dialog.show(supportFragmentManager, "MinConfidenceDialog")
                true
            }
            R.id.action_flip_camera -> {
                // Here only write the change into the preferences. The actual flip will
                // be performed by the OnSharedPreferenceChangeListener
                val oldVal = sharedPref!!.getBoolean(
                        getString(R.string.take_front_camera_key),
                        false
                ) // default is back camera

                val editor: SharedPreferences.Editor = sharedPref!!.edit()
                editor.putBoolean(getString(R.string.take_front_camera_key), !oldVal)
                editor.apply()
                true
            }

            R.id.action_help -> {
                // start activity with help text
                val i = Intent(applicationContext!!, HelpActivity().javaClass)
                this.startActivity(i)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun hasCamera(front: Boolean): Boolean {
        val cameraInfo: Camera.CameraInfo = Camera.CameraInfo()
        val numberOfCameras: Int = Camera.getNumberOfCameras()
        for (i in 0 until numberOfCameras) {
            Camera.getCameraInfo(i, cameraInfo)
            if (cameraInfo.facing == if (front) Camera.CameraInfo.CAMERA_FACING_FRONT else Camera.CameraInfo.CAMERA_FACING_BACK) {
                return true
            }
        }
        return false
    }
}






