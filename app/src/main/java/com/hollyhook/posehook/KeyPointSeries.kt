package com.hollyhook.posehook

import android.graphics.Color
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.PointsGraphSeries
import java.util.*


/**
 * combines PointsGraphSeries for x and y
 */
class KeyPointSeries {
    val dataSeriesX = PointsGraphSeries<DataPoint>()
    val dataSeriesY = PointsGraphSeries<DataPoint>()
    private var color = Color.argb(127, 180, 40, 0)
    var bodyPart: BodyPart = BodyPart.NOSE

    // keep order in sync with the context menu

    init {
        dataSeriesX.size = 7.0f
        dataSeriesX.shape = PointsGraphSeries.Shape.X
        dataSeriesX.color = color
        dataSeriesX.title = "x" // legend

        dataSeriesY.size = 7.0f
        dataSeriesY.shape = PointsGraphSeries.Shape.POINT
        dataSeriesY.color = color
        dataSeriesY.title = "y" // legend
    }

    // chain-able function
    fun color(color: Int): KeyPointSeries {
        this.color = color
        dataSeriesX.color = color
        dataSeriesY.color = color
        return this
    }

    // chain-able function
    fun bodyPart(bp: BodyPart): KeyPointSeries {
        this.bodyPart = bp
        return this
    }

    fun strokeWidth(sw: Float): KeyPointSeries {
        dataSeriesX.strokeWidth = sw
        dataSeriesY.strokeWidth = sw
        return this
    }


    /** this function also scales the data to double */
    fun appendData(now: Date, x: Float, y: Float) {
        dataSeriesX.appendData(
            DataPoint(now, x.toDouble()),
            true, DATA_LEN
        )
        dataSeriesY.appendData(
            DataPoint(now, y.toDouble()),
            true, DATA_LEN
        )
    }

    fun size(sz: Float): KeyPointSeries {
        dataSeriesX.size = sz
        dataSeriesY.size = sz
        return this
    }

    fun setInLegend(inLegend: Boolean): KeyPointSeries {
        dataSeriesX.isInLegend = inLegend
        dataSeriesY.isInLegend = inLegend
        return this
    }

    fun bodyPartName(s: String): KeyPointSeries {
        dataSeriesX.title = "$s x"
        dataSeriesY.title = "$s y"
        return this
    }
}