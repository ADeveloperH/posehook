package com.hollyhook.posehook

import android.view.Surface
import com.google.mediapipe.formats.proto.LandmarkProto
import java.util.*

/**
 * a list which holds all landmark points x,y,z,visibility,presence and a timestamp
 * Corrects the x,y according to the screen orientation at time of creation
 *
 */
data class Measurement(
    val mirror: Boolean,
    var argCombined: ArrayList<Any> = ArrayList(
        NUM_OF_VALUES_PER_LANDMARK * BodyPart.NUM_OF_BODY_PARTS.ordinal + 1
    )
) {

    /**
     * append points to the internal list. x,y is corrected according to current
     * screen rotation
     * @param landmark as received from the model
     * @param orientation is one of the [Surface] ROTATION_0 .. ROTATION_270 values
     */
    fun addLandmark(landmark: LandmarkProto.NormalizedLandmark, orientation: Int?) {

        val x = if (landmark.hasX()) landmark.x else Float.NaN
        val y = if (landmark.hasY()) landmark.y else Float.NaN

        when (orientation) {
            Surface.ROTATION_90 -> {
                argCombined.add(if (mirror) 1-y else y)
                argCombined.add(1 - x)
            }
            Surface.ROTATION_180 -> {
                argCombined.add(if (mirror) x else 1 - x)
                argCombined.add(1 - y)
            }
            Surface.ROTATION_270 -> {
                argCombined.add(if (mirror) y else 1 - y)
                argCombined.add(x)
            }
            else -> {
                argCombined.add(if (mirror) 1-x else x)
                argCombined.add(y)
            }
        }

        argCombined.add(if (landmark.hasZ()) landmark.z else Float.NaN)
        if (NUM_OF_VALUES_PER_LANDMARK > 3)
            argCombined.add(if (landmark.hasVisibility()) landmark.visibility else Float.NaN)
        if (NUM_OF_VALUES_PER_LANDMARK > 4)
            argCombined.add(if (landmark.hasPresence()) landmark.presence else Float.NaN)
    }

    // append a Float for the timestamp
    fun addTimestamp(t:Float) {
        argCombined.add(t) // a Long doesn't work, the js lib cannot deal with it

    }

    fun getX(bp: BodyPart): Float {
        return if (argCombined.size > bp.ordinal * NUM_OF_VALUES_PER_LANDMARK) {
            return argCombined[bp.ordinal * NUM_OF_VALUES_PER_LANDMARK] as Float
        } else Float.NaN
    }

    fun getY(bp: BodyPart): Float {
        return if (argCombined.size > bp.ordinal * NUM_OF_VALUES_PER_LANDMARK) {
            return argCombined[bp.ordinal * NUM_OF_VALUES_PER_LANDMARK + 1] as Float
        } else Float.NaN
    }

    private fun getZ(bp: BodyPart): Float {
        return if (argCombined.size > bp.ordinal*NUM_OF_VALUES_PER_LANDMARK) {
            return argCombined[bp.ordinal*NUM_OF_VALUES_PER_LANDMARK+2] as Float
        } else Float.NaN
    }

    private fun getVisibility(bp: BodyPart): Float {
        if (NUM_OF_VALUES_PER_LANDMARK < 4) return Float.NaN
        return if (argCombined.size > bp.ordinal * NUM_OF_VALUES_PER_LANDMARK) {
            return argCombined[bp.ordinal * NUM_OF_VALUES_PER_LANDMARK + 3] as Float
        } else Float.NaN
    }

    // no idea what presence actually is meant for
    private fun getPresence(bp: BodyPart): Float {
        if (NUM_OF_VALUES_PER_LANDMARK < 5) return Float.NaN
        return if (argCombined.size > bp.ordinal * NUM_OF_VALUES_PER_LANDMARK) {
            return argCombined[bp.ordinal * NUM_OF_VALUES_PER_LANDMARK + 4] as Float
        } else Float.NaN
    }

    // can'T use array of primitives due to the fact that the osc lib
    // wants a list at the interface. But still don't like mallocs
    fun getLandmark(bp: BodyPart): ArrayList<Float> {
        val argSingle: ArrayList<Float> = ArrayList(NUM_OF_VALUES_PER_LANDMARK)
        argSingle.add(getX(bp))
        argSingle.add(getY(bp))
        argSingle.add(getZ(bp))
        if (NUM_OF_VALUES_PER_LANDMARK > 3)
            argSingle.add(getVisibility(bp))
        if (NUM_OF_VALUES_PER_LANDMARK > 4)
            argSingle.add(getPresence(bp))
        return argSingle
    }



}