package com.hollyhook.posehook

import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

/**
 * Show info about anton anton wtf is anton?
 */
class HelpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        // Be sure to call the super class.
        super.onCreate(savedInstanceState)

        // See assets/res/any/layout/dialog_activity.xml for this
        // view layout definition, which is being set here as
        // the content of our screen.
        setContentView(R.layout.activity_help)

        // get version number
        val versionNumber: String = try {
            val pkg = packageName
            packageManager.getPackageInfo(pkg, 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            "?"
        }

        // fill in version number
        val helptext = getString(R.string.help_text)
        val tv = findViewById<View>(R.id.help_text) as TextView
        tv.text = Html.fromHtml(
            "<h2>poseHook</h2><div align=\"center\"><small>version "
                    + versionNumber
                    + "</small></div>"
                    + helptext
        )
        val t2 = findViewById<View>(R.id.contributions) as TextView
        t2.movementMethod = LinkMovementMethod.getInstance()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_help, menu)
        return true
    }

    // Handle main menu item selection
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {

            R.id.action_close_help -> {
                // close the activity with help text
                this.finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}