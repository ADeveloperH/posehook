package com.hollyhook.posehook

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.DialogFragment


class OSCConfigDialog : DialogFragment() {
    private val TAG = "OSCAddressDialog"

    // Use this instance of the interface to get members of the parent
    private lateinit var parent: MainActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            parent = context as MainActivity
        } catch (e: ClassCastException) {
            Log.e(TAG, "invalid parent class")
        }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val factory = LayoutInflater.from(context)
        // add a custom layout to an AlertDialog
        val oscConfigView: View = factory.inflate(R.layout.dialog_config_osc, null)

        val mirrored = parent.sharedPref!!.getBoolean(
            getString(R.string.mirror_key),
            MIRROR_OSC_DEFAULT
        )
        val switch1 = oscConfigView.findViewById(R.id.mirror_osc) as SwitchCompat?
        switch1?.isChecked = mirrored

        val combined = parent.sharedPref!!.getBoolean(
            getString(R.string.osc_combined_key),
            COMBINE_OSC_DEFAULT
        )
        val b: Button? = oscConfigView.findViewById(R.id.osc_address_button)
        b?.isClickable = !combined // disable if combined
        b?.isEnabled = !combined

        b?.setOnClickListener {
            dialog?.dismiss()
            // button was clicked, so we don't have combined osc, right
            val editor: SharedPreferences.Editor = parent.sharedPref!!.edit()
            editor.putBoolean(getString(R.string.osc_combined_key), false)
            editor.apply()
            // open the dialog for changing individual addresses

            val dialog = OSCAddressDialog()
            dialog.show(parent.supportFragmentManager, "OSCAddressDialog")
        }

        // editor for combined OSC address
        val str =  parent.sharedPref!!.getString(
            getString(R.string.combined_address_key),
            COMBINE_OSC_ADDRESS_DEFAULT
        )
        val ed = oscConfigView.findViewById(R.id.combined_address) as EditText?
        ed?.setText(str)
        ed?.isEnabled = combined

        (oscConfigView.findViewById(R.id.combined_address_long) as TextView).isEnabled = combined
        (oscConfigView.findViewById(R.id.combined_address_short) as TextView).isEnabled = combined

        val switch2 = oscConfigView.findViewById(R.id.combine_osc) as SwitchCompat?
        switch2?.isChecked = combined
        switch2?.setOnCheckedChangeListener { _, isChecked ->
            // true if the switch is in the On position
            b?.isClickable = !isChecked
            b?.isEnabled = !isChecked
            ed?.isEnabled = isChecked
            (oscConfigView.findViewById(R.id.combined_address_long) as TextView).isEnabled = isChecked
            (oscConfigView.findViewById(R.id.combined_address_short) as TextView).isEnabled = isChecked
        }

        return AlertDialog.Builder(context)
            .setView(oscConfigView)
            .setPositiveButton(R.string.alert_dialog_ok) { _, _ ->
                // ok button clicked
                val m = if (switch1 != null) switch1.isChecked else MIRROR_OSC_DEFAULT
                val c = if (switch2 != null) switch2.isChecked else COMBINE_OSC_DEFAULT
                val a = ed?.text

                // save values. this should trigger the listener
                val editor: SharedPreferences.Editor = parent.sharedPref!!.edit()
                editor.putBoolean(getString(R.string.mirror_key), m)
                editor.putBoolean(getString(R.string.osc_combined_key), c)
                Log.e(TAG, "combined $c")
                editor.putString(getString(R.string.combined_address_key), a.toString())
                editor.apply()
            }
            .create()

    }
}