package com.hollyhook.posehook

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.SeekBar
import androidx.fragment.app.DialogFragment
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt

class MinConfidenceDialog : DialogFragment() {
  private val TAG = "MinConfidence"

  // Use this instance of the interface to get members of the parent
  private lateinit var parent: MainActivity

  override fun onAttach(context: Context) {
    super.onAttach(context)
    try {
      parent = context as MainActivity
    } catch (e: ClassCastException) {
        Log.e(TAG, "invalid parent class")
    }
  }


  override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
    val factory = LayoutInflater.from(context)

    // add a custom layout to an AlertDialog
    val minConfView: View = factory.inflate(R.layout.dialog_confidence, null)

    val minConf = parent.sharedPref!!.getFloat(
      getString(R.string.min_confidence_key),
      getString(R.string.min_confidence_default).toFloat()
    )

    // seekBar goes from 0..7, confidence from 0.3 .. 0.99
    val sbp:Int = max(0, min((minConf*10).roundToInt()-3, 7))

    (minConfView.findViewById(R.id.seekBar) as SeekBar).progress = sbp

    return AlertDialog.Builder(context)
      .setView(minConfView)
      .setPositiveButton(R.string.alert_dialog_ok) { _, _ ->

        // ok button clicked
        val p = (minConfView.findViewById<View>(R.id.seekBar) as SeekBar).progress
        var mc = (p+3)/10.0f
        mc = max(0.3f, min(mc, 0.996f))
        // save values. this should trigger the listener
        // OnSharedPreferenceChangeListener in the fragment
        val editor: SharedPreferences.Editor = parent.sharedPref!!.edit()
        editor.putFloat(getString(R.string.min_confidence_key), mc)
        editor.apply()

      }
      .create()
  }
}