# poseHook Android App

### Overview

This is an app that continuously detects the body parts of a single person in the frames seen by
 your devices camera. It transmits the detected positions to a host in form of OSC messages.
 OSC is a UDP based protocol understood by many music or visual art software, e.g. p5js,
 TouchDesigner, Ableton Live (with Max for Live), Resolume Arena, etc.

![screen shot](doc/images/device-2021-05-15-185726.png)



### Download

At Google Play: [com.hollyhook.posehook](https://play.google.com/store/apps/details?id=com.hollyhook.posehook)

Latest apk is [here](https://bitbucket.org/constanze/posehook/src/master/app/release/app-release.apk)

### Demo

If you've installed poseHook v2 and are connected to the internet, you can see a multiuser life demo online. Send the OSC data to 18.192.229.240, and open http://ec2-18-192-229-240.eu-central-1.compute.amazonaws.com:8081 in the browser. Source code for the demo is available, ready to be used as a starting points for p5.js: https://bitbucket.org/constanze/posehook-demo/

![demo](doc/images/posehook.gif)


## How it works

The actual detection is done by the [MediaPipe Pose](https://google.github.io/mediapipe/solutions/pose) model. Notice that the app comes with the MediaPipe model integrated, thus no online transmission of personal data is performed. Each camera frame is given to the model. Only detection of a single persons body parts is supported. 

The positions of the body parts are in a range of 0 .. 1 for x and y position in the camera image,
with the top left corner of the image being 0,0. The values are plotted in the app. 
If you long-tap on the plot, you can select what body parts are to be shown in the graph. 
A detection rate of > 20fps is possible, depending on the performance of the phone.

The OSC message contain 4 floats per body part. 
 * x,y in the same range as the plot.
 * z represents the body part depth with the depth at the midpoint of hips being the origin, and the smaller the value the closer the landmark is to the camera. The magnitude of z uses roughly the same scale as x.
 * the fourth parameter is the visibility of the point. A value in [0.0, 1.0] indicating the likelihood of the landmark being visible (present and not occluded) in the image.

It is possible to configure if a single OSC message is sent, which combines all body parts in one message, or alternatively, poseHook can send OSC only for selected body parts. Go to Main Menu >> OSC configuration to configure which way the OSC shall be setup.

Further, the app can loop over 30 data points, which is convenient to setup the system. Tap on the "play" icon on top of the graph to replay the last 30 detected measurements in an endless loop. 

### Permissions

Camera permission is necessary obvsly. It appears that some devices camera implementation need external storage, therefore the app requests this as well, which solves stability issues. However, since the app itself doesn't use external storeage, for most devices it is possible to deny the permission, and see if it works without crashing on your device.
 
## Build the app 

* Clone or download this repository
* Open it in Android Studio. File >> Open...
* To build it, select menu  Build >> Build Apk(s)

NB: Due to size limitations, the mediapipe aar contains no x86 libraries, therefore running the app on the android emulator will not work. 

## Integrations & licenses

 - MediaPipe Pose, see https://github.com/google/mediapipe/blob/master/LICENSE
 - GraphView, see https://github.com/jjoe64/GraphView/blob/master/license.txt
 - JavaOSC, see https://github.com/hoijui/JavaOSC/blob/master/LICENSE.md
 - SLF4J, see http://www.slf4j.org/license.html
 
 poseHook itself is licensed under the Apache License 2.0

### Model used

The model is taken from MediaPipe 
 **[here](
 https://github.com/google/mediapipe/blob/master/mediapipe/modules/pose_landmark/pose_landmark_full.tflite)**.
Information about the model, incl. bias and ethical considerations are provided in this [Model Card](https://drive.google.com/file/d/10WlcTvrQnR_R2TdTmKw0nkyRLqrwNkWU/preview)
